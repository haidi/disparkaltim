<p style="text-align: center;"><strong>DATA 3A ( AMENITAS, AKSESIBILITAS DAN ATRAKSI) DESTINASI DI KALIMANTAN TIMUR 2019</strong></p>
<p style="text-align: center;">&nbsp;</p>
<!-- ************************************************************************** -->
<p>&nbsp;</p>
<!-- slide 1 -->
<p>&nbsp;</p>
<!-- ************************************************************************** -->
<table border="0" cellspacing="0"><colgroup width="25"></colgroup> <colgroup width="108"></colgroup> <colgroup width="71"></colgroup> <colgroup width="56"></colgroup> <colgroup width="99"></colgroup> <colgroup width="90"></colgroup> <colgroup width="64"></colgroup> <colgroup span="2" width="93"></colgroup> <colgroup width="86"></colgroup> <colgroup width="90"></colgroup> <colgroup width="93"></colgroup> <colgroup width="90"></colgroup> <colgroup span="2" width="93"></colgroup> <colgroup width="84"></colgroup> <colgroup width="12"></colgroup>
<tbody>
<tr>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="middle" bgcolor="#C5DFB4" height="30"><strong><span style="font-family: Arial; font-size: xx-small;">No</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="middle" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Daerah Tujuan Wisata</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Daya Tampung Wisatawan</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Jumlah Kunjungan</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Atraksi</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Aksesibilitas</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Amenitas</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Masyarakat yang Terlibat</span></strong></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Baik</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Sedang</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Buruk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Baik</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Sedang</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Buruk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Baik</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Sedang</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Buruk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Baik</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Sedang</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Buruk</span></strong></td>
</tr>
<tr>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle" height="22"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Bandara Kalimarau Berau.</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Jaringan PLTD,PLTS, PDAM, Cottages, Resort, homestay, Puskesmas, Bank Kaltimtara, Pos TNI AL, Telekomunikasi 4G</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Toilet Umum, kawasan rekreasi pantai umum dan sarprasnya, pengelolaan sampah, tempat ibadah</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="5" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom" height="11"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Jalur darat Tj. Redeb - Tj.</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top" height="36"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">500 orang/hari</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">P Derawan [Pantai, diving, snorkeling, kuliner laut, pulau gusung, whale shark]</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; font-size: 10px; font-family: Arial; text-align: center;" align="center" valign="top"><span style="color: #000000;">Batu beberapa titik jalan<br />rusak<br />Jalur laut/ Sungai [speed boat: Tj.Batu-Derawan,</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Pokdarwis Derawan, Pengelola Homestay, Investor</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">aksi sapta pesona</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom" height="11"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Tarakan-Derawan, Tj</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle" height="17"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Redep-Derawan]</span></td>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="middle" height="62"><strong><span style="color: #000000; font-family: Arial; font-size: xx-small;">1</span></strong></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Kepulauan Derawan Kabupaten Berau dan sekitarnya</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">600 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">P Maratua [Pantai, Diving, snorkeling, kuliner laut, Penyu]</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="font-size: 10px; font-family: Arial; border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">Bandara Maratua, Bndra Kalimarau<br />.<br />Jalur Laut [speed boat derawan-maratua, Tj.redep maratua]</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Bank Kaltimtara teluk harapan, Puskesmas Teluk Harapan, PLTD, homestay, penginapan, resort</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">PLTS, Telekomunikasi, Toilet Umum, sarpras anjungan pantai - umum, pengelolaan sampah, tempat ibadah</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">PDAM</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">mayarakat pengelola homestay, dan rumah makan</span></td>
<td style="font-size: 10px; font-family: Arial; border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">Inisiasi pembentukan Pokdarwis<br />aksi sapta pesona</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top" height="37"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">150 orang/ hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">P Kakaban (Snorkeling, Diving, Ubur-ubur air tawar, Danau]</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="font-size: 10px; font-family: Arial; border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">Jalur Laut : Speedboat dr Maratua, Derawan, Tj.<br />Batu</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Dermaga Sisi Laut dan Danau, Gazebo, Toilet, Listrik tenaga surya</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Jaringan Telekomunikasi, Air bersih</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Masyarakat Payung- Payung Maratua, Pemilik lahan ??</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top" height="53"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">250 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">P Sangalaki [Pantai, penangkaran penyu, kuliner laut]</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="font-size: 10px; font-family: Arial; border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">Jalur Laut : Speedboat dr Maratua, Derawan, Tj.<br />Batu</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Dermaga Pulau</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Fasilitas toilet umum, anjungan pantai dan sarpras umum, akomodasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Balai Konservasi Sumber Daya Alam (BKSDA)</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">aksi sapta pesona</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" height="12"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#FFFF00"><strong><em><span style="font-family: Trebuchet MS; font-size: xx-small;">Destinasi Penunjangnya</span></em></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Mangrove Tj. Batu Kab Berau</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">200 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Ekosistem mangrove, teduh</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top" height="38"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="right" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Museum Batiwakkal,</span></strong></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-top: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="3" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="right" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Sambaliung, Tj. Redep,</span></strong></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">250 orang/hari</span></td>
<td style="border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Sejarah kerajaan</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Tepian segah</span></strong></td>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-bottom: 1px solid #000000; border-center: 1px solid #000000; border-right: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Kampung Merasa, Kec. kelay</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">250 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Desa Budaya, Penangkaran orang utan</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Telaga Biru Kec. Batu Putih</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">200 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Telaga, sejuk alam</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="right" valign="middle"><strong><span style="font-family: Arial; font-size: xx-small;">Whale shake talisayan</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">100 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Hiu Paus</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Teluk Sulaiman, Biduk Biduk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">300 orang/ hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="font-size: 10px; font-family: Arial; border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Lanscap pantai yg teduh</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Dermaga, toilet, gazebo, home stay</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">telekomunikasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Danau Labuan Cermin, Biduk-Biduk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">150 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Danau air dua rasa, segar, kuliner, lanscape</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Dermaga, toilet, gazebo, home stay</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">telekomunikasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Pulau Kaniungan Besar, Biduk Biduk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">300 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="font-size: 10px; font-family: Arial; border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">Lanscape pulau yg tenang,<br />kuliner, aktifitas alam pulau dan pantai</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Resort, home stay, dermaga</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">telekomunikasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">toilet umum</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">masyarakat</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="24"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Teluk Sumbang, Biduk- Biduk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">300 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Lanscape pantai tenang, kuliner laut</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Jalur laut biduk-biduk - teluk sumbang</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Jalur biduk-biduk teluk sumbang (2 jam)</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Resort</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><strong><span style="font-family: Arial; font-size: xx-small;">Pegunungan merabu</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">150 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Lanscape ketinggian, desa budaya</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F" height="11"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" height="42"><strong><span style="color: #000000; font-family: Arial; font-size: xx-small;">2</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Pantai Manggar - Kota Balikpapan</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">100 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Pantai dengan keindahan deburan ombak dan pasir putih</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Tersedia fasilitas umum dan sosial yang cukup memadai</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" height="12"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#FFFF00"><strong><em><span style="font-family: Trebuchet MS; font-size: xx-small;">Destinasi Penunjangnya</span></em></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border-center: 1px solid #000000;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
</tbody>
</table>
<!-- ************************************************************************** -->
<!-- slide 2 -->
<table cellspacing="0" border="0">
	<colgroup width="25"></colgroup>
	<colgroup width="108"></colgroup>
	<colgroup width="71"></colgroup>
	<colgroup width="56"></colgroup>
	<colgroup width="99"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="64"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="86"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="84"></colgroup>
	<colgroup width="12"></colgroup>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 height="31" align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>No</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daerah Tujuan Wisata</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daya Tampung Wisatawan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Jumlah Kunjungan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Atraksi</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Aksesibilitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Amenitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Masyarakat yang Terlibat</font></b></td>
		<td style="border-left: 1px solid #000000" rowspan=19 align="left" valign=top><font face="Arial" size=1>KET</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="45" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><b><font face="Arial" size=1>Mangrove Center dan habitatnya - Graha Indah Balikpapan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>100 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>bekantan, ekosistem mangrove dekat kota</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Toilet, telekomunikasi, perahu eksplore mangrove</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>parkir</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Masyarakat, sekitar destinasi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="57" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="Arial" size=1>Penangkaran Buaya Teritip</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Tempat penangkaran buaya, budidaya dan bisnis kulit buaya makanan khas menu utama daging buaya</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Tersedia angkutan umum dari pusat kota</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="71" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><b><font face="Arial" size=1>KWPLH (Kawasasan wisata pendidikan dan lingkungan hidup)</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Kawasan Konservasi yang meliputi hutan bebas untuk pemeliharaan, pengembangbiakan dan penyembuhan hewan beruang madu</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Jalan poros smd bpp Km 23</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Toilet, lamin, parkir</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Telekomunikasi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="49" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Kebun Raya Balikpapan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Kolam pembibitan ikan dan tempat penelitian flora dan fauna</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="11" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=8 height="232" align="center" valign=middle sdval="3" sdnum="1033;0;0"><b><font face="Arial" size=1 color="#000000">3</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=8 align="left" valign=middle><b><font face="Arial" size=1>Sungai Mahakam &ndash; Kota Samarinda</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=8 align="center" valign=middle><font face="Arial" size=1>+/- 15000 orang sepanjang garis sungai 4 km smd kota dan smd seberang</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=8 align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Kapal Wisata [6 unit]</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jalan utama tepian</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Dermaga pasar pagi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Hotel di smd seberang blm dikelola dengan baik</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>blm adanya transportasi khusus wisata di smd seberang dari dermaga ke point wisata</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>pokdarwis?</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>Taman tepian kantor gubernur</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>Transportasi umum [angkot]</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Dermaga smd seberang</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>warung kuliner</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>taman tepian islamic center</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Taman Lampion</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Kawasan pasar pagi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Citra Niaga</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Oleh-oleh Amplang</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="12" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#FFFF00"><b><i><font face="Trebuchet MS" size=1>Destinasi Penunjangnya</font></i></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="62" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="Arial" size=1>Islamic Center Samarinda</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>wisata religi, tempat ibadah terbesar dan termegah di Kota Samarinda serta terbesar di Asia tenggara</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jalan utama tepian</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>toilet, klinik, sekolah, pusat kegiatan / acara keagamaan</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>?</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="61" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Desa Budaya Pampang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Wisata budaya dayak kayan berbaiagai atraksi kesenian seperti tari burung enggang dan tari leleng</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Kampung Tenun</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>Pusat kerajinan sarung samarinda</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
</table>
<!-- slide 3 -->
<!-- ************************************************************************** -->
<table border="0" cellspacing="0"><colgroup width="25"></colgroup> <colgroup width="106"></colgroup> <colgroup width="72"></colgroup> <colgroup width="56"></colgroup> <colgroup width="98"></colgroup> <colgroup width="93"></colgroup> <colgroup width="64"></colgroup> <colgroup width="93"></colgroup> <colgroup width="90"></colgroup> <colgroup width="86"></colgroup> <colgroup width="90"></colgroup> <colgroup width="93"></colgroup> <colgroup width="90"></colgroup> <colgroup span="2" width="93"></colgroup> <colgroup width="83"></colgroup> <colgroup width="12"></colgroup>
<tbody>
<tr>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="middle" bgcolor="#C5DFB4" height="31"><strong><span style="font-family: Arial; font-size: xx-small;">No</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="middle" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Daerah Tujuan Wisata</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Daya Tampung Wisatawan</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Jumlah Kunjungan</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Atraksi</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Aksesibilitas</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Amenitas</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Masyarakat yang Terlibat</span></strong></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Baik</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Sedang</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Buruk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Baik</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Sedang</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Buruk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Baik</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Sedang</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Buruk</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Baik</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Sedang</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" bgcolor="#C5DFB4"><strong><span style="font-family: Arial; font-size: xx-small;">Buruk</span></strong></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><strong><span style="font-family: Arial; font-size: xx-small;">Citra Niaga</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">wisata belanja pasar citra niaga</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F" height="13"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="52"><strong><span style="color: #000000; font-family: Arial; font-size: xx-small;">4</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Pulau Kumala Tenggarong - Kabupaten Kutai Kartanegara</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">350 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">wisata tematik, tempat ngumpul rekreasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" height="12"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#FFFF00"><strong><em><span style="font-family: Trebuchet MS; font-size: xx-small;">Destinasi Penunjangnya</span></em></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" height="39"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Museum Mulawarman Tenggarong</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">250 orang/ hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Sejarah budaya kalimantan timur</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Planetarium Tenggarong</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">150 orang/ hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Rekreasi keluarga, edukasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Taman tepian tenggarong</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">300 orang/ hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">rekreasi, tempat ngumpul</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" height="75"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><strong><span style="font-family: Arial; font-size: xx-small;">Festival Erau</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="font-size: 10px; font-family: Arial; text-align: center; border: 1px solid #000000;" align="center" valign="top"><span style="color: #000000;">tradisi<br />budaya Indonesia yang dilaksanakan setiap tahun dengan pusat kegiatan di kota Tenggarong, Kutai Kartanegara,</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><strong><span style="font-family: Arial; font-size: xx-small;">Waduk Panji Sukarame</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">200 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">tempat rekreasi, lanscape danau, perahu dayung</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="31"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Museum Kayu Sukarame</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">200 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Wisata sejarah sumberdaya alam kaltim</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" height="69"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Desa Wisata Pela, Kecamatan Kota Bangun, Kabupaten Kutai Kartanegara</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">250 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="font-size: 10px; font-family: Arial; text-align: center; border: 1px solid #000000;" align="center" valign="top"><span style="color: #000000;">Pesut Mahakam, kehidupan desa nelayan sungai, lanscape sunset danau semayang<br />tanjung tamanoh (wisata tematik)</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Jalur darat samarinda - tenggarong - kota bangun</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Jalur penyembrangan sungai kota bangun - desa pela</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Jaringan telekomunikasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Homestay, souvenir, warung/toko, kuliner, air konsumsi rumah tangga, listrik</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">sistem air limbah masyarakat</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Pokdarwis Desa Pela, komunitas wisata</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">aksi sapta pesona</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" height="50"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Desa Budaya Kedang Ipil, Kota Bangun</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">200 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Festival budaya 3 kali setahun, kerajinan rotan, air terjun landai, lanscape desa budaya pedalaman kutai</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Akses Samarinda - Tenggarong Kota Bangun</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Akses jalan utama kota bangun ke kedang ipil</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Home stay</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Kuliner, tempat lapang, toilet umum di perkampungan, sistem air bersih, jaringan telekomunikasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Pokdarwis, masyarakat desa</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F" height="8"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" bgcolor="#001F5F"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="52"><strong><span style="color: #000000; font-family: Arial; font-size: xx-small;">5</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">Situs Pegunungan Karts Sangkulirang Mangkalihat - Kabupaten Kutai Timur dan Berau</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">250 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" rowspan="2" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Goa Mengkuris, sejarah purba</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Jalan poros Samarinda - Sangata/ Berau - Sangata</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Jalan penghubung Kec. Kaubun - Karangan</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Lahan Parkir</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Home Stay, Fasum, air bersih, toilet, gazebo, PLN, souvenir/ pasar, faskes, telekomunikasi</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Bupati Kutim 2017 TDK mengijinkan Tambang dan HTI dikawasan cagar.</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Masyarakat desa batu lepoq</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" height="41"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Keindahan lanscap pegunungan karst dan perkampungan desa dan alam</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom" height="12"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" colspan="3" align="center" valign="top" bgcolor="#FFFF00"><strong><em><span style="font-family: Trebuchet MS; font-size: xx-small;">Destinasi Penunjangnya</span></em></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="bottom"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top" height="45"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><strong><span style="font-family: Arial; font-size: xx-small;">TNK Prevab Mentoko - Kabupaten Kutai Timur</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">15 orang/hari</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="font-family: Arial; font-size: xx-small;">Penangkaran orang utan alam, penelitian</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="font-family: Arial; font-size: xx-small;">Masyarakat desa Prevab Mentoko, Kementerian kehutanan (TNK)</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="top"><span style="color: #000000;">&nbsp;</span></td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle" height="25"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><strong><span style="font-family: Arial; font-size: xx-small;">Pohon Kayu Ulin</span></strong></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
<td style="border: 1px solid #000000; text-align: center;" align="center" valign="middle"><span style="color: #000000;">&nbsp;</span></td>
</tr>
</tbody>
</table>
<!-- table-4 -->
<table cellspacing="0" border="0">
	<colgroup width="25"></colgroup>
	<colgroup width="108"></colgroup>
	<colgroup width="71"></colgroup>
	<colgroup width="56"></colgroup>
	<colgroup width="99"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="64"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="86"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="93"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="84"></colgroup>
	<colgroup width="12"></colgroup>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 height="31" align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>No</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daerah Tujuan Wisata</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daya Tampung Wisatawan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Jumlah Kunjungan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Atraksi</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Aksesibilitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Amenitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Masyarakat yang Terlibat</font></b></td>
		<td style="border-left: 1px solid #000000" rowspan=22 align="left" valign=top><font face="Arial" size=1>KET</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>TNK sangkima</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>150 orang/ hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>Kayu khas hutan Kalimantan</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>Toilet, Telekomunikasi, parkir</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>TNK</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Pantai Teluk Lombok, Sangata</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>300 orang/ hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>lanscape pantai - teluk</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Desa budaya miau baru, kombeng</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>200 orang/ hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Kehidupan desa suku dayak</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>aktifitas budaya</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>Masyarakat desa miau baru</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Pantai Sekerat, bengalon</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>250 orang/ hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>lanscape pantai</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Air Terjun Batu Lapis</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Biduk-Biduk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>300 orang/ hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; font-size: 10px; font-family: Arial; text-align: center;" align="left" valign=top><font color="#000000">Pantai, danau, kuliner laut,<br>ketenangan kehidupan nelayan laut</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="9" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="51" align="center" valign=middle sdval="6" sdnum="1033;0;0"><b><font face="Arial" size=1 color="#000000">6</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><b><font face="Arial" size=1>Komplek Lamin dan adat budaya di Tanjung Isuy - Kutai Barat</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="12" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#FFFF00"><b><i><font face="Trebuchet MS" size=1>Destinasi Penunjangnya</font></i></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="45" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Lamin adat Mancong</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>250 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>Kehidupan suku dayak, Lamin, danau jempang</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>jalur darat samarinda- kukar-kubar</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>akses darat Jalur utama kota bangun melak - menuju lokasi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>home stay lamin</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>tempat kumpul, toilet, sistem air bersih, jaringan listrik, telekomunikasi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>masyarakat kampung</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="41" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><b><font face="Arial" size=1>Taman Kresik Luway</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>250 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Anggrek Hitam</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Air terjun jantur inar</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Taman Budaya Sendawar</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Air Terjun Jantur Mapan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Batu dinding Mahakam ulu</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="10" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="71" align="center" valign=middle sdval="7" sdnum="1033;0;0"><b><font face="Arial" size=1 color="#000000">7</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Museum Sadurengas - Kabupaten Paser</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>300 orang / hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdval="2400" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">2400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>sejarah edukasi dan budaya kaltim</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Jalur Darat  roda 2 dan roda 4 , menuju obyek sekitar 10 menit perjalanan dari ibukota Tana Paser menuju Desa Pasir Belengkong</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Taman Depan Museum, Taman Bermain Anak, Jalan Setapak Siring, Kursi Taman Siring Tidak ada</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Pokdarwis Desa Pasir Belengkong Kec. Pasir Belengkong</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="12" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#FFFF00"><b><i><font face="Trebuchet MS" size=1>Destinasi Penunjangnya</font></i></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Goa Tengkorak Batu Kajang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>100 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Pemakaman purba</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>parkir, telekomunikasi, toilet</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="74" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Air Terjun Doyam Turu</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>500 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdval="200" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">200</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Tarian budaya paser</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Jalur Darat  roda 2 dan roda 4 Jalur Darat menuju obyek sekitar 1 Jam perjalanan dari ibukota Tana Paser menuju Desa Lempesu Kec Pasir Belengkong</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Taman Bermain Anak, Jalan Setapak, Gazebo, Jembatan 5 unit rusak parah</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Pokdarwis Desa Lempesu, Kec. Pasir Belengkon</font></td>
		</tr>
</table>
<!-- ************************************************************************** -->
<!-- table-5 -->
<table cellspacing="0" border="0">
	<colgroup width="25"></colgroup>
	<colgroup width="108"></colgroup>
	<colgroup width="71"></colgroup>
	<colgroup width="56"></colgroup>
	<colgroup width="99"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="64"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="86"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="93"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="84"></colgroup>
	<colgroup width="12"></colgroup>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 height="31" align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>No</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daerah Tujuan Wisata</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daya Tampung Wisatawan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Jumlah Kunjungan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Atraksi</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Aksesibilitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Amenitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Masyarakat yang Terlibat</font></b></td>
		<td style="border-left: 1px solid #000000" rowspan=9 align="left" valign=top><font face="Arial" size=1>KET</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="108" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><b><font face="Arial" size=1>Kampung Warna Warni Desa Janju, Kecamatan Tanah Grogot (Wisata Buatan)</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=middle><font face="Arial" size=1>300 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>pemandangan alam pesisir pantai yang menawan dengan hiasan warna warni di sepanjang jembatan dan rumah penduduk dan kuliner serta produk khas warga lokal dan menampilkan seni dan budaya lokal ramah dalam melayani</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Dapat ditempuh dengan roda 2 dan roda 4, setelah sampai dapat melalui, Jalur Darat menuju Paser 15 menit perjalanan dari ibukota tana paser menuju Desa Janju Kec. Tanah Grogot</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih,</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Gazebo, panggung kesenian, pergola, tempat ibadah, kios, taman, toilet, jalan setapak, lampu taman, tempat bermain anak</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>dermaga wisata dan perahu wisata tidak ada</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Pokdarwis Desa Janju, Kec Tanah Grogot</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="60" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Tahura Lati</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=middle><font face="Arial" size=1>1000 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle sdval="2800" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">2800</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Penangkaran Rusa yang terpelihara dengan baik bekerja sama dengan DLH</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Jalur Darat  roda 2 dan roda 4, Jalur Darat menuju Paser 1 Jam perjalanan dari ibukota Tana Paser ke Desa Saing Prupuk</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih,Jalur track lari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Gazebo, Taman Bermain, Kursi, perahu bebek</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Pokdarwis Desa Saing Prupuk Kec. Batu Engau</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="68" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Kemilau Laut Pondong</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=middle><font face="Arial" size=1>400 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle sdval="900" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">900</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Lomba Perahu</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Tarian budaya paser</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Jalur Darat  roda 2 dan roda 4, Jalur Darat menuju obyek wisata sekitar 30 menit perjalanan dari ibukota Tana Paser Menuju Desa Pondong</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih,Jalur track lari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Produk UKM</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Lantai Jembatan, Bangunan ada yg miring,</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Pokdarwis Desa Pondong, Kec. Kuaro</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="81" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Doyam Seriam</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>50 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle sdval="56" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">56</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Suasana kesejukan pesona alam alami dengan 17 tingkat air terjun dan memiliki kolam</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Jalur Darat  roda 2 dan roda 4 (Double) Jalur Darat menuju obyek sekitar 1 Jam perjalanan dari ibukota Tana Paser, menuju Desa Modang, Kec. Kuaro</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>, Hotel, minimarket, air bersih</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>toilet, musholla, Taman Bermain Anak, perlu perlengkapan safety panjat tebing, baju pelampung, rambu penunjuk arah, dan papan informasi tidak ada</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Pokdarwis Desa Modang Kec. Kuaro</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="82" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Danum layong</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=middle><font face="Arial" size=1>300 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle sdval="400" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>wisata alam dengan air yang mengandung air panas yang terus mengalir, dengan pemandangan alam yang menaik</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Dapat ditempuh dengan roda 2 dan roda 4, Jalur Darat menuju Paser 1 Jam perjalanan dari ibukota Tana Paser ke Kec. Longkali</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Gazebo, pompa air, panggung, pergola, taman, toilet rusak, penunjuk arah, papan informasi, kios</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Pokdarwis Kelurahan Longkali Kec. Longkali</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="112" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Liang Mangku Langit</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=middle><font face="Arial" size=1>100 0rang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle sdval="100" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">100</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Pesona alam pada obyek ini berbeda dengan lainnya karena terdapat tebing dan tempat mandi seorang putri yang bisa membuat awer muda dari kisahnya</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; font-size: 10px; font-family: Arial; text-align: center;" align="center" valign=top><font face="Arial" size=1>Dapat ditempuh dengan roda 2 dan roda 4, setelah sampai dapat melalui perahu dan darat, Jalur Darat menuju Paser 1,5 Jam perjalanan dari ibukota tana paser menuju desa Muara Kuaro Kec. Muara Komam</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=bottom><font face="Arial" size=1>Jaringan Telekomunikasi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Perahu wisata, Gazebo, dermaga wisata, jembatan titian, jembatan 3 unit rusak, toilet, mushollah</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Pokdarwis Desa Muara Kuaro Kec. Muara Komam</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="107" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Goa Losan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=middle><font face="Arial" size=1>100 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle sdval="200" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">200</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Pesona alam pada obyek ini berbeda dengan lainnya karena terdapat tebing pesona goa yang bercahaya kristal dengan stalakmit seorang putri yang bisa membuat awer muda dari kisahnya</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; font-size: 10px; font-family: Arial; text-align: center;" align="center" valign=top><font face="Arial" size=1>Dapat ditempuh dengan roda 2 dan roda 4, setelah sampai dapat melalui perahu dan darat, Jalur Darat menuju Paser 1,5 Jam perjalanan dari ibukota tana paser menuju desa Muara Kuaro Kec. Muara Komam</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Gazebo, panggung, pergola, tempat ibadah, kios, taman, toilet rusak parak</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Pokdarwis Desa Batu Butok, Kec. Muara Komam</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
</table>
<!-- ************************************************************************** -->
<!-- table-6 -->
<table cellspacing="0" border="0">
	<colgroup width="25"></colgroup>
	<colgroup width="108"></colgroup>
	<colgroup width="71"></colgroup>
	<colgroup width="56"></colgroup>
	<colgroup width="99"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="64"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="86"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="93"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="84"></colgroup>
	<colgroup width="12"></colgroup>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 height="31" align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>No</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daerah Tujuan Wisata</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daya Tampung Wisatawan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Jumlah Kunjungan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Atraksi</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Aksesibilitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Amenitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Masyarakat yang Terlibat</font></b></td>
		<td style="border-left: 1px solid #000000" rowspan=18 align="left" valign=top><font face="Arial" size=1>KET</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="79" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Goa Tengkorak</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>50 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdval="500" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">500</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Pesona goa obyek ini berbeda dengan lainnya karena didalamnya terdapat tengkorak kepala manusia yang tersimpat rapi dan menjadi cagar budaya di kabupaten paser</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; font-size: 10px; font-family: Arial; text-align: center;" align="center" valign=top><font color="#000000">Dapat ditempuh dengan<br>roda 2 dan roda 4, setelah sampai dapat melalui Jalur Darat menuju Paser 1 Jam perjalanan dari ibukota tana paser menuju Desa Kesunge Kec. Batu Sopang</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Gazebo, pompa air, panggung, pergola, tempat ibadah, kios, taman, toilet rusak parak, pagar</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Pokdarwis Desa Kesunge, Kec Batu Sopang</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="113" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Goa Loyang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>50 orang/hari</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdval="400" sdnum="1033;0;0"><font face="Arial" size=1 color="#000000">400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Pesona goa obyek ini berbeda dengan lainnya karena didalamnya terdapat stalaktit dan stalakmit dengan banyak pintu goa yang bercabang dan luas dengan pemandangan yang menawan di kabupaten paser</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Dapat ditempuh dengan roda 2 dan roda 4, setelah sampai dapat melalui, Jalur Darat menuju Paser 1 Jam perjalanan dari ibukota tana paser menuju Desa Kesunge Kec. Batu Sopang</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Jaringan Telekomunikasi, Hotel, minimarket, air bersih</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Gazebo, pompa air, panggung, pergola, tempat ibadah, kios, taman, toilet rusak parak,tangga naik, pagar</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Pokdarwis Desa Kesunge, Kec Batu Sopang</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="9" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="61" align="center" valign=middle sdval="8" sdnum="1033;0;0"><b><font face="Arial" size=1 color="#000000">8</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="Arial" size=1>Pantai Tanjung Jumlai - Penajam Paser Utara</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>pepohonan kelapa yang tumbuh di sepanjang bentangan pantai</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Banana Boat, flying fox, tempat duduk sepanjang pantai, wc umum, penyewaan ban, kios penjual makanan</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>dermaga runtuh dikarenakan abrasi</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=4 height="129" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#FFFF00"><b><i><font face="Trebuchet MS" size=1>Destinasi Penunjangnya</font></i></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><b><font face="Arial" size=1>Penangkaran Rusa Kabupaten Penajam Paser Utara</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>ratusan ekor rusa dari jenis Rusa Sambar (Cervus Unicolor Brokei) dan Rusa Timor (Cervus Timorensis)</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>meeting room, guest house, musholla, gazebo, dan laboratorium</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><b><font face="Arial" size=1>Ekowisata Mangrove Penajam ( salam konservasi)</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Tembinus Agathis</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="10" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="center" valign=middle sdval="9" sdnum="1033;0;0"><b><font face="Arial" size=1 color="#000000">9</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Mangrove Brebas Pantai Kota Bontang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="12" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#FFFF00"><b><i><font face="Trebuchet MS" size=1>Destinasi Penunjangnya</font></i></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="37" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Kehidupan Masyarakat di Bontang Kuala</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Perkampungan yang didirikan di atas permukaan laut</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="57" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Pulau Beras Basah</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Pulau Cantik dengan keelokan pantai pasir putih, ada mercusuar menjulang tinggi dan pemandangan bawah laut yang menawan</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="51" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Taman Wisata Graha Mangrove</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>Wisata alam dengan hutan bakau besar, rindang dengan fasilitas lengkap</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; font-size: 10px; font-family: Arial; text-align: center;" align="center" valign=top><font color="#000000">Puluhan gazebo, ruang<br>pertemuan/resto dengan kapasitas 500 orang, rumah pohon, fotoboth, banana boat, bebek- bebekan, perahu, dan</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="11" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#001F5F"><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="45" align="center" valign=middle sdval="10" sdnum="1033;0;0"><b><font face="Arial" size=1 color="#000000">10</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><b><font face="Arial" size=1>Batu Tenvang Kabupaten Mahakam Ulu</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>batu kapur putih yang membentuk dinding di tepi Sungai</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
</table>
<!-- ************************************************************************** -->
<!-- table-7 -->
<table cellspacing="0" border="0">
	<colgroup width="25"></colgroup>
	<colgroup width="108"></colgroup>
	<colgroup width="71"></colgroup>
	<colgroup width="56"></colgroup>
	<colgroup width="99"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="64"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="86"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup width="93"></colgroup>
	<colgroup width="90"></colgroup>
	<colgroup span="2" width="93"></colgroup>
	<colgroup width="84"></colgroup>
	<colgroup width="21"></colgroup>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 height="31" align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>No</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daerah Tujuan Wisata</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Daya Tampung Wisatawan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Jumlah Kunjungan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Atraksi</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Aksesibilitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Amenitas</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Masyarakat yang Terlibat</font></b></td>
		<td style="border-left: 1px solid #000000" rowspan=7 align="left" valign=top><font face="Arial" size=1>KET</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Baik</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Sedang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top bgcolor="#C5DFB4"><b><font face="Arial" size=1>Buruk</font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="12" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top bgcolor="#FFFF00"><b><i><font face="Trebuchet MS" size=1>Destinasi Penunjangnya</font></i></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Riam Panjang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font face="Arial" size=1>titik lokasi jeramnya panjang sekali</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="44" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Riam Udang</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=top><font face="Arial" size=1>riam yang konturnya menyerupai lekukan udang, dengan medan terberat (menanjak)</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Arial" size=1>Air Terjun Keneheq</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="25" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Arial" size=1>Lamin adat dan Budaya setempat</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="Arial" size=1>Hudoq</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000" height="40" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="right" valign=bottom><font face="Arial" size=1>Samarinda,</font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=top><font color="#000000"><br></font></td>
		<td align="left" valign=top><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td colspan=17 height="46" align="right" valign=top><font face="Arial" size=1>Kabid Pengembangan Destinasi Pariwisata,</font></td>
		</tr>
	<tr>
		<td colspan=17 height="46" align="right" valign=middle><b><font face="Arial" size=1>Drs. Ahmad Herwansyah, M.Si</font></b></td>
		</tr>
	<tr>
		<td colspan=17 height="13" align="right" valign=top><font face="Arial" size=1>19680204 198803 1 003</font></td>
		</tr>
</table>
<!-- ************************************************************************** -->