@extends('website._layouts.default')

@section('script-top')
    <style>
        img, .zoom{
            cursor: pointer;
        }
        .icon-show{
            transform: translate(0px, 18px);
        }
        .mfp-content img.mfp-img {
            max-height: 500px;
            width: 100%;
            height: 500px;
        }
    </style>
@endsection

@section('script-bottom')
    <script>
        $(function(){
            $('.more').click(function(){
                console.log('terklik')
            });
        })
    </script>
@endsection

@section('content')
<!-- block-wrapper-section
    ================================================== -->
    <section class="block-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <!-- block content -->
                    <div class="block-content">

                        <!-- single-post box -->
                        <div class="single-post-box">

                            <div class="title-section">
                                <h1><span class="world">Foto</span></h1>
                            </div>

                            <div class="article-inpost">
                                @foreach ($data as $index => $item)
                                    <div class="col-md-3">
                                        <div class="image-content">
                                            <div class="image-place zoom" href="{{url($item->preview_url)}}">
                                                {!! $item->preview_original !!}
                                                <div class="hover-image">
                                                    <a class="zoom" href="{{url($item->preview_url)}}">
                                                        <i class="fa fa-arrows-alt icon-show"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                        <!-- End single-post box -->
                        <div class="center-button">
                            <a href="#"><i class="fa fa-refresh"></i> Lebih Banyak Foto</a>
                        </div>
                        <br><br>

                        <div class="center-button">
                            <a href="javascript:void(0)" class="more"><i class="fa fa-refresh" style="font-size:20px"></i> Lebih Banyak Foto</a>
                        </div>

                    </div>
                    <!-- End block content -->

                    

                </div>
                

            </div>
            

        </div>
    </section>
    <!-- End block-wrapper-section -->
@endsection