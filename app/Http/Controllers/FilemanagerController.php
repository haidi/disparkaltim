<?php 
namespace App\Http\Controllers;

use Image;

use App\Web\Models\File;
use Illuminate\Http\Request;

class FilemanagerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

	public function postUpload($key='file')
    {
        $file = $this->request->file($key);

        $mime = $file->getMimeType();
        $name = sprintf('%s.%s', str_random(), $file->getClientOriginalExtension());

        if(str_is('image/*', $mime)){
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();

            if($width > 800){
                $image->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            if($height > 1000){
                $image->resize(null, 1000, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $attribute = [
                'name'      => $file->getClientOriginalName(),
                'extension' => $file->getClientOriginalExtension(),
                'mime'      => $mime,
                'size'      => $image->filesize(),
                'height'    => $height,
                'width'     => $width,
            ];

            $type = 'image/tinymce';
            $path = 'storages/' . $type;
            
            $dir = public_path(dirname($path.'/'.$name));
            if( ! file_exists($dir) ) mkdir($dir, 0777, true);

            $image->save(public_path($path . '/' . $name));

        }else{
            $attribute = [
                'name'      => $file->getClientOriginalName(),
                'extension' => $file->getClientOriginalExtension(),
                'mime'      => $file->getClientMimeType(),
                'size'      => $file->getClientSize()
            ];
            $type = 'file';
            $path = 'storages/' . $type;
            $file->move(public_path($path), $name);
        }

        $mFile            = new File;
        $mFile->name      = $name;
        $mFile->type      = $type;
        $mFile->path      = $path;
        $mFile->attribute = $attribute;

        $mFile->save();
        $data = $mFile;
        $success = true;

        return compact('success', 'data');
    }

    public function getMceUpload()
    {
        return view('uploads.dialog');
    }

    public function postMceUpload()
    {
        $upload = $this->postUpload('imagefile');
        $file_path = $upload['data']->url;
        return view('uploads.upload', compact('file_path'));
    }
}