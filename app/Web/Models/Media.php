<?php 
namespace App\Web\Models;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

	public $timestamps = false;
	protected $table = 'social_media';
	
	public function scopeSorted($query, $by='id', $sort='ASC')
	{
		return $query->orderBy($by, $sort);
	}

	public function scopeSearch($query, $by, $key)
    {
        return $query->where($by, 'LIKE', '%'.$key.'%');
	}

	public function setShare($url)
	{
		if($this->name == 'facebook'){
			return 'https://www.facebook.com/sharer/sharer.php?u='.$url.'&display=popup';
		}elseif($this->name == 'twitter'){
			return 'https://twitter.com/intent/tweet?url='.$url;
		}else{
			return '#';
		}
	}
}